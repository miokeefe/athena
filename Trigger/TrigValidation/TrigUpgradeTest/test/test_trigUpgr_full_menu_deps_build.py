#!/usr/bin/env python

# art-description: Test with SGInputLoader.FailIfNoProxy=True to check for unmet data dependencies
# art-type: build
# art-include: master/Athena
# Skipping art-output which has no effect for build tests.
# If you create a grid version, check art-output in existing grid tests.

from TrigValTools.TrigValSteering import Test, ExecStep, CheckSteps

ex = ExecStep.ExecStep()
ex.type = 'athena'
ex.job_options = 'TrigUpgradeTest/full_menu.py'
ex.input = 'data'
ex.args = '-c "isOnline=True;doWriteBS=False;doWriteRDOTrigger=True;failIfNoProxy=True;"'
ex.skip_events = 10

test = Test.Test()
test.art_type = 'build'
test.exec_steps = [ex]
test.check_steps = CheckSteps.default_check_steps(test)

import sys
sys.exit(test.run())
